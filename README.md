# APNS Dissector

A [Wireshark](https://wireshark.org/) dissector
for the Apple Push Notification Service protocol,
This is the protocol Apple devices use to get push notifications from Apple servers,
*not* the protocol third party app servers use to send notifications to Apple.

![Screenshot](doc/screenshot.png)

The protocol was initially documented
in the [pushproxy](https://github.com/mfrister/pushproxy) project,
but the documentation is quite incomplete.
Also, pushproxy uses a man-in-the-middle proxy,
which involves creating certificates,
getting apsd to connecting to your server,
bypassing certificate pinning, etc.
Capturing traffic passively is much simpler.

Note that there are multiple versions of the APNS protocol.
They are distinguished via [ALPN](https://en.wikipedia.org/wiki/Application-Layer_Protocol_Negotiation) in TLS.
Currently only `apns-security-v3` is supported.
The newer `apns-pack-v1` protocol is not yet supported,
and the [older protocol from iOS4](https://github.com/mfrister/pushproxy/blob/master/doc/apple-push-protocol-ios4.md) was not tested.

## Usage
<!-- this needs a lot more detail-->

- Compile this project and copy apns.so into the Wireshark plugin directory.

- Dump TLS session keys from the device so you can decrypt the traffic.
  You can use my [SSLKeyLog](https://gitlab.com/nicolas17/sslkeylog) tweak on jailbroken iOS.

- Make apsd downgrade to the older "non-packed" protocol,
  by installing the [APNSNoPack](https://gitlab.com/nicolas17/apnsnopack) tweak.

- Capture network traffic from the device.
  I'm personally using hostapd on my computer to create a Wi-Fi access point
  and bridging it to the Internet, then running Wireshark on the same computer.
  The [remote virtual interface](https://developer.apple.com/documentation/network/recording_a_packet_trace) may be another option.

- Configure Wireshark TLS preferences to use your TLS key log file.

## Sample capture

A [sample capture file](doc/sample-capture.pcapng) and its corresponding [TLS keylog file](doc/sample-capture-ssl.log) is available in the doc directory.
